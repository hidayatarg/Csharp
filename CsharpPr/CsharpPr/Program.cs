﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpPr
{
    public class Person
    {
        public int Age;
    }


    class Program   
    {
        static void Main(string[] args)
        {
            var number = 1;
            Increment(number);
            //even we send number variable to the increment method a copy of the varible is send but the orignally number with main 
            //scope is 1.
            Console.WriteLine(number); //will show 1 will not be affected

            //reference value
            //var person= new Person();
            //person.Age = 20;                OR

            var person = new Person() {Age = 20};
            MakeOld(person);

            Console.WriteLine(person.Age);


            Console.ReadKey();

        }

        //in increment method within scope again have a number But both are different places in memory
        public static void Increment(int number)
        {
            //this method takes a parameter called number and this variable is limited within the scope of increment method.
            number += 10;
        }

        public static void MakeOld(Person person)
        {
            person.Age += 10;
        }

    }
}
